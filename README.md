`npm i --save koa-sse-slim`

```js
const Koa = require('koa')
const sse = require('koa-sse-slim')

const app = new Koa()

app.use(sse({
	ping: 60000 // ms to do keep alive ping, default is 60 seconds
}))
app.use(async ctx=>{
	ctx.sse.send('success') //will send no name event with data: success
	ctx.sse.send({event: 'greeting', data: 'hello there!'}) //will send greeting event with data: hello there!
	ctx.sse.send({event: 'meat', data: {id: 1, name: 'test'}}) //will send meat event with stringified JSON you'll have to parse in the client
	//sends undefined data as empty string
	//sends error objects by doing toString()
	//will stringify everything else, numbers, arrays, etc
	ctx.sse.sendClose('goodbye') //sends {event: 'close', data: 'goodbye'}
	//if you send close event or client closes connection, kills sse and frees up resources

	//if you never use ctx.sse object it never makes sse or pools
	//If you ever call ctx.sse you MUST close the connection however
})
```
